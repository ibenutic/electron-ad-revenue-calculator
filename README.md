# What is this application?

It's a simple Ad revenue calculator for mobile game developers. It's based on a ElectronJS + ReactJS combination.

# How to run this application?

On a PC with NodeJS 16 installed run a command

```bash
npm i
```

After the install process is done start the app using a command:

```bash
npm start
```

If you want to build this project please consult the [Electron Forge documentation](https://www.electronforge.io/config/makers).

After figuring out all the dependancies on your PC run a command:

```bash
npm run make
```

## Icon licence

<a href="https://www.flaticon.com/free-icons/gamepad" title="gamepad icons">Gamepad icons created by Freepik - Flaticon</a>
