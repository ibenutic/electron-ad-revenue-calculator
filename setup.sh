#!/bin/bash -e
#Check OS and install dependencies
OS=/etc/os-release
if grep -q Fedora $OS; then
	sudo dnf install dpkg fakeroot flatpak flatpak-builder elfutils rpm rpm-build -y
	fi
if grep -q Ubuntu $OS; then
	sudo apt install dpkg fakeroot flatpak flatpak-builder elfutils rpm rpm-build -y
	fi
#add flathub repo
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo



#install flatpak stuff

flatpak install io.atom.electron.BaseApp/x86_64/20.08 io.atom.electron.BaseApp/x86_64/stable io.atom.electron.BaseApp/x86_64/19.08 -y
flatpak install org.electronjs.Electron2.BaseApp/x86_64/stable -y
