import { useState, useEffect } from "react";
import React from "react";
import Slider from "./components/Slider";
import "./Home.css";
const Home = () => {
  const [games, setGames] = useState(0);
  const [avgPlayTime, setAvgPlayTime] = useState(0);
  const [numberOfPlayers, setNumberOfPlayers] = useState(0);
  const [cpm, setCpm] = useState(0);
  const [sessionsPerDay, setSessionsPerDay] = useState(0);
  const [adsPerMin, setAdsPerMin] = useState(0);
  const [fillRate, setFillRate] = useState(0);
  const [includeTaxes, setIncludeTaxes] = useState(false);
  const [includeGoogleCut, setIncludeGoogleCut] = useState(false);
  const [earnings, setEarnings] = useState(0);
  const [taxes, setTaxes] = useState(0);
  const [googleCut, setGoogleCut] = useState(0);

  useEffect(() => {
    const impressions = numberOfPlayers * sessionsPerDay * games;
    const impressions_cpm = (impressions / 1000) * cpm;
    const adRev = avgPlayTime * adsPerMin * (fillRate / 100);
    const gC = includeGoogleCut ? 1 - googleCut / 100 : 1
    const tax = includeTaxes ? 1 - taxes / 100 : 1
    const finalDayly = impressions_cpm * adRev * tax * gC;
    setEarnings(parseInt(finalDayly));
  }, [
    games,
    avgPlayTime,
    numberOfPlayers,
    cpm,
    sessionsPerDay,
    adsPerMin,
    fillRate,
    includeGoogleCut,
    includeTaxes,
    googleCut,
    taxes
  ])
  return (
    <div className="home">
      <div className="top"></div>
      <div className="container">
        <div className="sliders">
          <div className="left">
            <Slider
              name="Games"
              value={games}
              min={1}
              max={200}
              setValue={setGames}
            />
            <Slider
              name="Players per game"
              value={numberOfPlayers}
              setValue={setNumberOfPlayers}
              max={100000}
              step={50}
              min={50}
              def={500}
              append="Players"
            />
            <Slider
              name="Sessions per day"
              append="sessions"
              max={20}
              def={2}
              value={sessionsPerDay}
              setValue={setSessionsPerDay}
              min={1}
            />
            <Slider
              name="Average session duration"
              min={2}
              value={avgPlayTime}
              setValue={setAvgPlayTime}
              append="Minutes"
            />
            <Slider
              name="Ads per minute"
              step={0.5}
              def={2}
              max={6}
              min={0.5}
              value={adsPerMin}
              setValue={setAdsPerMin}
              append="Ads per minute"
            />
          </div>
          <div className="right">
            <Slider
              name="eCPM"
              step={0.01}
              min={0.2}
              max={50}
              value={cpm}
              setValue={setCpm}
              def={4.96}
              append="$"
            />

            <Slider
              name="Fill rate"
              value={fillRate}
              setValue={setFillRate}
              def={96}
              append="%"
            />
            <Slider
              name="Taxes"
              value={taxes}
              setValue={setTaxes}
              def={25}
              append="%"
            />
            <Slider
              name="Google Cut"
              value={googleCut}
              append="%"
              def={30}
              setValue={setGoogleCut}
            />
            <h2>Taxes</h2>

            <div className="checky">
              <input
                type="checkbox"
                onChange={() => setIncludeTaxes((x) => !x)}
              />
              <label>Include Taxes</label>
            </div>

            <div className="checky">
              <input
                type="checkbox"
                onChange={() => setIncludeGoogleCut((x) => !x)}
              />
              <label>Include Google's cut</label>
            </div>
          </div>
        </div>
        <div className="result">
          <p>
            <b>$ {earnings.toLocaleString()}</b> /day
          </p>
          <p>
            <b>$ {(parseInt(earnings) * 30).toLocaleString()}</b> /month
          </p>
          <p>
            <b>{parseInt(earnings * 30 * 7.33).toLocaleString()}</b> kn/month
          </p>
        </div>
      </div>
      
    </div>
  );
};

export default Home;
