import { useEffect } from "react"
import React from "react"

const Slider = props => {
    const { name, value, setValue, min, max, step, def, append } = props

    const onChange = e => {
        setValue(e.target.value)
    }
    useEffect(() => {
        if (min) setValue(min)
        if (def) setValue(def)
    }, [def, min, setValue])
    return <div className="slider">
        <label>{name}</label>
        <input type="range" min={min || 1} max={max || 100} step={step || 1} value={value} onChange={onChange} />
        <div className="info">
            <span><input type="number" value={value} onChange={onChange} className="value" /> </span>
            <span>{append || ''}</span>
        </div>
    </div>

}

export default Slider